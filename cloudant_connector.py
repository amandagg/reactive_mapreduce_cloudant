#
# Cloudant Connection Class
#
# TODO: Change from Cloudant library to HTTP requests directly

import configuration
from cloudant.client import Cloudant
from cloudant.adapters import Replay429Adapter

class CloudantConnection(object):

    def __init__(self):
        self.config = configuration.get_cloudant_default()
        self.username = self.config['service_username']
        self.password = self.config['service_password']
        self.url = self.config['service_url']
        self.db = self.config['service_database']
        self.service_name = self.config['service_name']
        self.client = Cloudant(self.username, self.password, url=self.url, adapter=Replay429Adapter(retries=100, initialBackoff=0.01))
        self.client.connect()
        print(self.config)


    def upload(self, doc):
        dbase = self.client[self.db]
        '''if doc['_id'] in dbase:
            dbase[doc['_id']].delete()'''
        dbase.create_document(doc)

    def target_db(self):
        return self.db

    def target_service_name(self):
        return self.service_name

    def cleanup_db(self):
        dbase = self.client[self.db]
        for doc in dbase:
            doc.delete()


    def close_connection(self):
        self.client.disconnect()