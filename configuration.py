#
# Basic configuration methods
#
import os
import json
import yaml

"""Copied from PyWren"""
def load(config_filename):

    res = yaml.safe_load(open(config_filename, 'r'))
    if 'pwcl' in res and res['pwcl']['storage_bucket'] == '<BUCKET_NAME>':
        raise Exception(
            "{} has bucket name as {} -- make sure you change the default container".format(
                config_filename, res['pwcl']['storage_bucket']))

    if res['ibm_cf']['endpoint'] == '<CF_API_ENDPOINT>':
        raise Exception(
            "{} has CF API endpoint as {} -- make sure you change the default CF API endpoint".format(
                config_filename, res['ibm_cf']['endpoint']))
    if res['ibm_cf']['namespace'] == '<CF_NAMESPACE>':
        raise Exception(
            "{} has namespace as {} -- make sure you change the default namespace".format(
                config_filename, res['ibm_cf']['namespace']))
    if res['ibm_cf']['api_key'] == '<CF_API_KEY>':
        raise Exception(
            "{} has CF API key as {} -- make sure you change the default CF API key".format(
                config_filename, res['pwcl']['api_key']))

    if res['ibm_cos']['endpoint'] == '<COS_API_ENDPOINT>':
        raise Exception(
            "{} has CF API endpoint as {} -- make sure you change the default COS API endpoint".format(
                config_filename, res['pwcl']['storage_container']))

    return res


def get_cf_default():
    config_filename = os.path.join(os.path.expanduser("~/.pwcl_config"))
    config_data = load(config_filename)
    return config_data['ibm_cf']

def get_cloudant_default():
    config_filename = os.path.join(os.path.expanduser("~/.pwcl_config"))
    config_data = load(config_filename)
    return config_data['ibm_cloudant']
