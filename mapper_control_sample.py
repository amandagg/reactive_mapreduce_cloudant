from cloudant.client import Cloudant
from requests.exceptions import HTTPError
from cloudant.adapters import Replay429Adapter


def map_function(value):
    return value + 1


def store_result(mapper_id, result):
    serviceUsername = "b33d4865-2afe-44ee-b61b-f00de8abdbcf-bluemix"
    servicePassword = "73f8931e6403d36ccca3a3a6075afb861b39f9b465984231992e6bbc4b562758"
    serviceURL = "https://b33d4865-2afe-44ee-b61b-f00de8abdbcf-bluemix:73f8931e6403d36ccca3a3a6075afb861b39f9b465984231992e6bbc4b562758@b33d4865-2afe-44ee-b61b-f00de8abdbcf-bluemix.cloudant.com"
    db_name = "first_database"
    status_doc = "mapper_evol"

    client = Cloudant(serviceUsername, servicePassword, url=serviceURL,
                      adapter=Replay429Adapter(retries=100, initialBackoff=0.01))
    client.connect()
    database = client[db_name]

    # Upload results
    mapper_doc = "mapper" + str(mapper_id) + "_status"
    mapper_info = database[mapper_doc]
    mapper_info["current_status"] = "finished"
    mapper_info["result"] = result
    mapper_info.save()

    # Update counter
    current_status = database[status_doc]

    while (1):
        try:
            print(current_status)
            current_status["finished"] = current_status["finished"] + 1
            current_status.save()
        except HTTPError as error:
            if error.response.status_code == 409:
                client.disconnect()
                client.connect()
                database = client[db_name]
                current_status = database[status_doc]
                print("409 error")
                continue
            else:
                raise
        break
    print(current_status)

    client.disconnect()


def main(dict):
    mapper_id = dict['mapper_id']
    value = dict['values']
    result = map_function(value)
    store_result(mapper_id, result)

    return {'message': 'done'}