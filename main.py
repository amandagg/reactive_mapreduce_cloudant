#
# Main program executing against IBM Cloud
#

import invoker

def map_function(x):
    return x+1

def reduce_function(x, y):
    return x+y


if __name__ == "__main__":

    data = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150]
    client = invoker.CloudInvoker()

    client.map_reduce(map_function, reduce_function, data)