#
# Cloud Functions Connection Class
#

import os
import base64
import requests
import configuration

class CloudFunctionsConnection(object):

    def __init__(self):
        self.config = configuration.get_cf_default()
        self.endpoint = self.config['endpoint'].replace('http:', 'https:')
        self.namespace = self.config['namespace']
        self.api_key = str.encode(self.config['api_key'])
        self.auth = base64.encodestring(self.api_key).replace(b'\n', b'')
        self.headers = {
            'content-type': 'application/json',
            'Authorization': 'Basic %s' % self.auth.decode('UTF-8')
        }

        self.session = requests.session()
        self.session.headers.update(self.headers)


    def create_function(self, code, function_name):
        url = os.path.join(self.endpoint, 'api', 'v1', 'namespaces', self.namespace, 'actions', function_name + "?overwrite=True")
        cfexec['kind'] = 'python'
        cfexec['code'] = code
        data['exec'] = cfexec

        res = self.session.put(url, json=data, headers=self.headers)


    def invoke_function(self, function_name, payload):
        url = os.path.join(self.endpoint, 'api', 'v1', 'namespaces', self.namespace, 'actions', function_name)
        print("url will be "+url+" and payload "+str(payload))
        response = self.session.post(url, json=payload)
        print(response)

