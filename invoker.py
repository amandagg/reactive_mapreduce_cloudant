#
# Main setup and invoker
#

import cloudfunctions_connector as cf_connector
import cloudant_connector

class CloudInvoker(object):

    def __init__(self):
        self.cf_client = cf_connector.CloudFunctionsConnection()
        self.cloudant_client = cloudant_connector.CloudantConnection()

    def map_reduce(self, map_function, reduce_function, data):

        '''__Prepare environment__'''
        mapper_num = len(data)

        '''Cloudant cleanup'''
        self.cloudant_client.cleanup_db()

        '''Cloudant Changes feed design doc'''
        filter_design = {
            '_id' : '_design/feedfiltering',
            'filters' : {
                'by_status' : 'function(doc, req) { if ((doc._id == "mapper_evol") && (doc.finished == doc.to_process)) {return true;} return false; }'
            }
        }
        self.cloudant_client.upload(filter_design)

        '''Cloudant control doc'''
        status_doc = {
            '_id': 'mapper_evol',
            'to_process': mapper_num,
            'finished': 0
        }
        print(status_doc)
        self.cloudant_client.upload(status_doc)

        '''IBM Cloud Functions'''
        '''añadir: MAP a partir de los datos, llamar a la funcion, guardar resultado en cloudant o s3 al acabar.REDUCE obtener datos y guardar resultado'''

        '''IBM Cloud Trigger'''
        triggering_db = self.cloudant_client.target_db()
        triggering_feed = "/_/"+self.cloudant_client.target_service_name()+"/changes"
        print("bx wsk trigger create mapDoneTrigger --feed "+triggering_feed+" --param dbname "+triggering_db+" --param filter feedfiltering/by_status")

        '''IBM Cloud Rule'''
        print("bx wsk rule create ruleDoneMap mapDoneTrigger reduce_function")


        '''__Invoking map stage__'''
        for i in range (0, mapper_num):
            mapper_doc = {
                "_id": "mapper" + str(i+1) + "_status",
                "current_status": "pending",
                "result": ""
            }
            self.cloudant_client.upload(mapper_doc)

        self.cloudant_client.close_connection()

        input("Press any key to continue")

        # TODO: unify 2 for loops
        for i in range(0, mapper_num):
            payload = {
                'mapper_id': i + 1,
                'values': data[i]
            }
            # TODO: automate the mapper function as parameter
            self.cf_client.invoke_function("mapper", payload)
            print(payload)



