from cloudant.client import Cloudant
import functools
from cloudant.adapters import Replay429Adapter

serviceUsername = "b33d4865-2afe-44ee-b61b-f00de8abdbcf-bluemix"
servicePassword = "73f8931e6403d36ccca3a3a6075afb861b39f9b465984231992e6bbc4b562758"
serviceURL = "https://b33d4865-2afe-44ee-b61b-f00de8abdbcf-bluemix:73f8931e6403d36ccca3a3a6075afb861b39f9b465984231992e6bbc4b562758@b33d4865-2afe-44ee-b61b-f00de8abdbcf-bluemix.cloudant.com"
db_name = "first_database"
status_doc = "mapper_evol"

cloudant_client = Cloudant(serviceUsername, servicePassword, url=serviceURL, adapter=Replay429Adapter(retries=100, initialBackoff=0.01))
cloudant_client.connect()
database = cloudant_client[db_name]


def get_intermediate_values():
    num_mappers = database[status_doc]["to_process"]
    values = []
    for i in range(0, num_mappers):
        id = "mapper" + str(i+1) + "_status"
        mapper_doc = database[id]
        values.append(mapper_doc['result'])
    return values


def reduce_function(first, second):
    return first + second


def store_result(result):
    result = {
        "_id": "final_result",
        "result": result
    }
    database.create_document(result)


def main(dict):
    intermediate_values = get_intermediate_values()
    print(intermediate_values)
    final_result = functools.reduce(reduce_function, intermediate_values, 0)
    print(final_result)
    store_result(final_result)

    return { "message": "done" }